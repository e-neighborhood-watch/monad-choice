# Changelog

## 0.2.0.0 (2020-03-27)
Fixed broken `MonadChoice` instance for `MonadRandom` types and fixed, expanded, and fine tuned dependency version requirements.

### Additions
- `UniformRandom`
  - Created marker `newtype` `UniformRandom` that provides a `MonadChoice` instance for members of the `MonadRandom` typeclass.
  - Added functions `lift` and `colift` to wrap and unwrap `UniformRandom`, respectively.
  - Added instance for `MonadChoice` using `uniform` from the `MoandRandom` package when the wrapped monad is a member of the `MonadRandom` class.
  - Added various passthrough instances
    - `Functor`
    - `Applicative`
    - `Monad`
    - `MonadRandom`
    - `MonadFix`
    - `MonadFail`
    - `Alternative`
    - `MonadPlus`
    - `MonadIO`
    - `Semigroup`
    - `Monoid`
    - `MonadError`
    - `MonadReader`
    - `MonadState`
    - `Foldable`
    - `Traversable`
    - `Eq1`
    - `Ord1`
    - `MonadZip`
    - `MonadCont`
    - `Eq`
    - `Ord`
    - `MonadRWS`
    - `MonadWriter`
    - `MonadSplit`
    - `PrimMonad`
    - `MonadInterleave`

### Removals
- `MonadChoice`
  - Removed previous instance for members of the `MonadRandom` type class since it overlapped with all other `MonadChoice` instances.
  - Removed passthrough instances for constant space `WriterT` and `RWST` if `transformers` version is less than 0.5.6

## 0.1.0.0 (2020-02-29)
Inital version

### Additions
- `MonadChoice`
  - Created `MonadChoice` typeclass to represent that allow choices to be made by some arbitrary entity.
  - Added function `chooseM`.
  - Added instances for the `ChoiceT` both covariant and invariant.
  - Added instance for `MonadRandom` instances by using the `uniform` function.
  - Added various passthrough instances
    - `MaybeT`
    - `ExceptT`
    - `IdentityT`
    - `StateT` either lazy or strict
    - `ReaderT`
    - `WriterT` either constant space, lazy, or strict
    - `AccumT`
    - `ContT`
    - `RWST` either constant space, lazy, strict
    - `SelectT`
- `ChoiceT`
  - Added two different versions of `ChoiceT`, one a covariant functor, the other an invariant functor on the category of monads.
  - Shared additions:
    - Function `runChoiceT`
    - Instances for `Functor`, `Applicative`, `Monad`, `MonadTrans`, and `MonadChoice`
    - Various passthrough instances
      - `MonadReader`
      - `MonadState`
      - `MonadWriter`
      - `MonadRWS`
      - `MonadIO`
  - `Covariant.ChoiceT`
    - Added functions `mapChoiceT` and `runBacktrackableChoiceT`.
  - `Invariant.ChoiceT`
    - Exposed constructor `ChoiceT`.
    - Added function `invmapChoiceT`.
    - Added various passthrough instances.
      - `Contravariant`
      - `Invariant`
      - `Alternative`
      - `MonadPlus`
      - `MonadError`
- `Choice`
  - Added two different versions of `Choice` which of type aliases for each `ChoiceT` over the `Identity` monad.
  - Added function `runChoice` for each version.
  - For `Covariant.Choice` added functions `runChoiceM` and `runBacktrackableChoiceT`.
