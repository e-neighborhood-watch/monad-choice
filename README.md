# The `monad-choice` package

This package provides a multipurpose monad transformer (and associated monad and class), to represent the idea of a result depending on the outcomes of a series of choices.

## Example

The following is a simple use of `MonadChoice` for creating the name of a berry.

    {-# Language FlexibleContexts #-}

    berry :: MonadChoice NonEmpty m => m String
    berry = do
      berryColor  <- choose $ "red"   :| ["blue", "orange", "yellow", "black"]
      berryFlavor <- choose $ "sweet" :| ["sour", "bitter"]
      (++ "berry") <$> choose ( berryColor :| [berryFlavor, berryColor ++ "-" ++ berryFlavor] )

This can be used with `MonadRandom` to create a random berry, used with `Gen` to create berries for unit tests, or with user input as the structure for a menu where the user selects a berry.
