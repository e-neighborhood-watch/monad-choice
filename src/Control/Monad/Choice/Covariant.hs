{-# LANGUAGE RankNTypes #-}

{-|
Module     : Control.Monad.Choice.Covariant
Copyright  : (c) Eamon Olive, 2020
             (c) Louis Hyde,  2020
License    : AGPL-3
Maintainer : ejolive97@gmail.com
Stability  : experimental

-}
module Control.Monad.Choice.Covariant
  ( Choice
  , runChoice
  , runChoiceM
  , runBacktrackableChoiceM
  ) where

-- Internal imports

import Control.Monad.Trans.Choice.Covariant
  ( ChoiceT
  , mapChoiceT
  , runBacktrackableChoiceT
  , runChoiceT
  )

-- External imports

import Control.Monad.Identity
  ( Identity
  , runIdentity
  )

type Choice f = ChoiceT f Identity

runChoice :: (forall x. f x -> x) -> Choice f a -> a
runChoice chooser = runIdentity . runChoiceT (pure . chooser)

runChoiceM :: Monad m => (forall x. f x -> m x) -> Choice f a -> m a
runChoiceM chooser = runChoiceT chooser . mapChoiceT (pure . runIdentity)

runBacktrackableChoiceM :: Monad m => (forall x. f x -> m (Maybe x)) -> Choice f a -> m (Maybe a)
runBacktrackableChoiceM chooser = runBacktrackableChoiceT chooser . mapChoiceT (pure . runIdentity)
