{-# LANGUAGE RankNTypes #-}

{-|
Module     : Control.Monad.Choice.Invariant
Copyright  : (c) Eamon Olive, 2020
             (c) Louis Hyde,  2020
License    : AGPL-3
Maintainer : ejolive97@gmail.com
Stability  : experimental

-}
module Control.Monad.Choice.Invariant
  ( Choice
  , runChoice
  ) where

-- Internal imports

import Control.Monad.Trans.Choice.Invariant
  ( ChoiceT
  , runChoiceT
  )

-- External imports

import Data.Functor.Identity
  ( Identity
    ( runIdentity
    )
  )

type Choice f a = ChoiceT f Identity a

runChoice ::
  (forall x . f x -> x)
    -> Choice f a
      -> a
runChoice chooser = runIdentity . runChoiceT (\options -> pure $ chooser options)
